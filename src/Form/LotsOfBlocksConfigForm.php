<?php

namespace Drupal\lots_of_blocks\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure lots of blocks settings for this site.
 */
class LotsOfBlocksConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'lots_of_blocks.settings';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lots_of_blocks_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    // Copy text details.
    $form['copy_text'] = [
      '#type' => 'details',
      '#title' => $this->t('Copy text block'),
      '#open' => FALSE,
    ];

    // Text format for copy text.
    $form['copy_text']['copy_text_filter_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Text format'),
      '#options' => $this->getTextFormats(),
      '#default_value' => $config->get('copy_text_filter_format') ?? 'basic',
      '#description' => $this->t('Select the text format to be used with the copy text block.'),
    ];

    // Blockquote details.
    $form['blockquote'] = [
      '#type' => 'details',
      '#title' => $this->t('Blockquote block'),
      '#open' => FALSE,
    ];

    // Text format for blockquote.
    $form['blockquote']['blockquote_filter_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Text format'),
      '#options' => $this->getTextFormats(),
      '#default_value' => $config->get('blockquote_filter_format') ?? 'basic',
      '#description' => $this->t('Select the text format to be used with the blockquote block.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      ->set('copy_text_filter_format', $values['copy_text_filter_format'])
      ->set('blockquote_filter_format', $values['blockquote_filter_format'])
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Function to get all current text formats.
   *
   * @return array
   *   Array of text formats (id => label).
   */
  private function getTextFormats(): array {

    // Get all the text formats.
    $tfs = $this->entityTypeManager->getStorage('filter_format')->loadMultiple();

    // Blank array in case there are no formats.
    $text_formats = [];

    // Step through each text format and get info.
    foreach ($tfs as $tf) {
      $text_formats[$tf->id()] = $tf->label();
    }

    return $text_formats;
  }

}
