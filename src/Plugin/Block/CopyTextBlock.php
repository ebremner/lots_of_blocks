<?php

namespace Drupal\lots_of_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Copy text block.
 *
 * @Block(
 *   id = "lob_cbl_copy_text",
 *   admin_label = @Translation("Copy text"),
 * )
 */
class CopyTextBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactory $configFactory
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // The render of the copy text using the text format.
    $copy_text = [
      '#type' => 'processed_text',
      '#text' => $this->configuration['copy_text']['value'],
      '#format' => $this->configuration['copy_text']['format'],
    ];

    // Return custom template with variable.
    return [
      '#theme' => 'lob_cbl_copy_text',
      '#copy_text' => $copy_text,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Get the config for lots of blocks.
    $config = $this->configFactory->getEditable('lots_of_blocks.settings');

    // The copy text element.
    $form['copy_text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Copy text'),
      '#cols' => 60,
      '#rows' => 5,
      '#format' => $config->get('copy_text_filter_format') ?? 'basic',
      '#default_value' => $this->configuration['copy_text'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Set the config for the copy text block.
    $this->configuration['copy_text'] = $values['copy_text'];
  }

}
