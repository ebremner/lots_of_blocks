<?php

namespace Drupal\lots_of_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Blockquote block.
 *
 * @Block(
 *   id = "lob_cbl_blockquote",
 *   admin_label = @Translation("Blockquote"),
 * )
 */
class BlockquoteBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactory $configFactory
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Get the config for this block.
    $config = $this->configuration;

    // The blockquote info.
    $blockquote = [
      'text' => [
        '#type' => 'processed_text',
        '#text' => $config['quote']['value'],
        '#format' => $config['quote']['format'],
      ],
      'attribution' => $config['attribution'],
    ];

    // Return custom template with variable.
    return [
      '#theme' => 'lob_cbl_blockquote',
      '#blockquote' => $blockquote,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Get the config for lots of blocks.
    $config = $this->configFactory->getEditable('lots_of_blocks.settings');

    // The blockquote text element.
    $form['quote'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Quote'),
      '#cols' => 60,
      '#rows' => 5,
      '#format' => $config->get('blockquote_filter_format') ?? 'basic',
      '#default_value' => $this->configuration['quote']['value'],
    ];

    // The attribution element.
    $form['attribution'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Attribution'),
      '#default_value' => $this->configuration['attribution'],
      '#description' => $this->t('Enter the attribution(author).'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Set the config for the blockquote block.
    $this->configuration['quote'] = $values['quote'];
    $this->configuration['attribution'] = $values['attribution'];
  }

}
